// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestGameMode.generated.h"

UCLASS(minimalapi)
class ATestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestGameMode();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Respawn)
	float SecondsToRespawn;

	void RespawnPlayer(AController* Controller);

private:
	UPROPERTY()
	TArray<AActor*> SpawnPoints;

	void HandleRespawnPlayer(AController* Controller);

protected:
	virtual void BeginPlay() override;
};



