// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FirstPersonCharacter.generated.h"

class UInputComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMagicChanged);

UCLASS(config=Game)
class AFirstPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen by all) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen by all) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Body mesh (body; seen only by the rest of players) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UStaticMeshComponent* BodyMesh;

	/** Magic area: expandable area where the magic take effect */
	UPROPERTY(VisibleDefaultsOnly, Category = Magic)
	class UStaticMeshComponent* MagicArea;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Health manager component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
	class UFirstPersonHealthComponent* HealthComponent;

public:
	AFirstPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AFirstPersonProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Magic)
	float TimeToChargeMagic;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Magic)
	float MagicCooldownTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Magic)
	float MagicAreaMaxScale;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Magic)
	float MagicDamage;

	UPROPERTY(BlueprintAssignable)
	FOnMagicChanged OnChargeMagic;

	UPROPERTY(BlueprintAssignable)
	FOnMagicChanged OnReleaseMagic;

private:
	UPROPERTY()
	bool bIsDead;

	UPROPERTY()
	float MagicCharge;

	UPROPERTY()
	float bIsChargingMagic;

	UPROPERTY()
	float bIsCoolingDown;

	UFUNCTION()
	void OnCharacterHealthChanged(float Health, float NormalizedHealth, float DeltaHealth);

	UFUNCTION()
	void ChargeMagic();

	UFUNCTION()
	void ReleaseMagic();

protected:
	/** Fires a projectile. */
	void OnFire();

	/** Fires a projectile on the server */
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_OnFire();

	/** Play fire animation and sound in all clients */
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Server_OnFireMulticast();

	/** Starts magic attack. */
	void OnMagic(float Value);

	/** Updates magic area on the server */
	UFUNCTION(Server, Reliable)
	void Server_UpdateMagic(float Scale);

	/** Fires magic on the server */
	UFUNCTION(Server, Reliable)
	void Server_ReleaseMagic();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

protected:
	virtual void BeginPlay();

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
	/** Returns FirstPersonHealthComponent subobject **/
	FORCEINLINE class UFirstPersonHealthComponent* GetHealthComponent() const { return HealthComponent; }
	/** Returns MagicAreaComponent subobject **/
	FORCEINLINE class UStaticMeshComponent* GetMagicArea() const { return MagicArea; }
};

