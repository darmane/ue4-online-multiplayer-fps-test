// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonHealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UFirstPersonHealthComponent::UFirstPersonHealthComponent()
{
	BaseHealth = 100.f;

	SetIsReplicated(true);
}


// Called when the game starts
void UFirstPersonHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = BaseHealth;

	// Only on the server
	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* Owner = GetOwner();
		if (Owner)
		{
			Owner->OnTakeAnyDamage.AddDynamic(this, &UFirstPersonHealthComponent::HandleOnTakeAnyDamage);
		}

		SendHealthChanged(Health, GetNormalizedHealth(), 0.f);
	}
}

void UFirstPersonHealthComponent::HandleOnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("DAMAGE RECEIVED %f"), Damage);

	if (Damage <= 0.f)
	{
		return;
	}

	Health = FMath::Max(0.f, Health - Damage);

	SendHealthChanged(Health, GetNormalizedHealth(), -Damage);
}

void UFirstPersonHealthComponent::Server_OnTakeAnyDamageMulticast_Implementation(float CurrentHealth, float NormalizedHealth, float DeltaHealth)
{
	OnHealthChanged.Broadcast(CurrentHealth, NormalizedHealth, DeltaHealth);
}

void UFirstPersonHealthComponent::SendHealthChanged(float CurrentHealth, float NormalizedHealth, float DeltaHealth)
{
	Server_OnTakeAnyDamageMulticast(CurrentHealth, NormalizedHealth, DeltaHealth);
	OnHealthChanged.Broadcast(CurrentHealth, NormalizedHealth, DeltaHealth);
}

float UFirstPersonHealthComponent::GetNormalizedHealth()
{
	return Health / BaseHealth;
}

float UFirstPersonHealthComponent::GetHealth()
{
	return Health;
}

void UFirstPersonHealthComponent::HealCharacter(float HealAmount)
{
	// Only on the server
	if (GetOwnerRole() == ROLE_Authority)
	{
		const float AddedHeal = FMath::Max(0.f, HealAmount);
		Health = FMath::Min(BaseHealth, Health + AddedHeal);

		SendHealthChanged(Health, GetNormalizedHealth(), HealAmount);
	}
}

void UFirstPersonHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFirstPersonHealthComponent, Health);
}