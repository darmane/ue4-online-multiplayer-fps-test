// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FirstPersonHUD.generated.h"

UCLASS()
class AFirstPersonHUD : public AHUD
{
	GENERATED_BODY()

public:
	AFirstPersonHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget)
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Widget)
	class UUserWidget* CurrentWidget;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	UFUNCTION()
	void OnPlayerRespawn();

};

