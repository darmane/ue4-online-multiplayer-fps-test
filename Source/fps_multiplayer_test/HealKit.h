// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HealKit.generated.h"

UCLASS()
class FPS_MULTIPLAYER_TEST_API AHealKit : public AActor
{
	GENERATED_BODY()

	/** Collision */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UBoxComponent* Collision;

	/** Mesh: Visual element */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UStaticMeshComponent* Mesh;
	
public:	
	// Sets default values for this actor's properties
	AHealKit();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Heal)
	float HealthAmount;

	UFUNCTION()
	virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
