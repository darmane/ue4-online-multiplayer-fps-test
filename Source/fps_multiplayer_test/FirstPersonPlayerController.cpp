// Fill out your copyright notice in the Description page of Project Settings.


#include "FirstPersonPlayerController.h"
#include "Net/UnrealNetwork.h"


void AFirstPersonPlayerController::ResetHUDOnRespawn()
{
	Server_OnPlayerRespawnMulticast();
}

void AFirstPersonPlayerController::Server_OnPlayerRespawnMulticast_Implementation()
{
	OnPlayerRespawn.Broadcast();
}

