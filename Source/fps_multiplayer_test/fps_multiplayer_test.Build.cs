// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class fps_multiplayer_test : ModuleRules
{
	public fps_multiplayer_test(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG", "Slate", "SlateCore" });
	}
}
