// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestGameMode.h"
#include "FirstPersonHUD.h"
#include "FirstPersonCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h" 
#include "GameFramework/PlayerStart.h" 
#include "TestGameMode.h" 
#include "FirstPersonPlayerController.h" 

ATestGameMode::ATestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFirstPersonHUD::StaticClass();

	// use our custom Player controller class
	PlayerControllerClass = AFirstPersonPlayerController::StaticClass();

	SecondsToRespawn = 10.f;
}

void ATestGameMode::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = GetWorld();
	if (World)
	{
		UGameplayStatics::GetAllActorsOfClass(World, APlayerStart::StaticClass(), SpawnPoints);
	}
}

void ATestGameMode::RespawnPlayer(AController* Controller)
{
	AFirstPersonPlayerController* PlayerController = Cast<AFirstPersonPlayerController>(Controller);
	if (Controller)
	{			
		FTimerDelegate TimerDelegate = FTimerDelegate::CreateUObject(this, &ATestGameMode::HandleRespawnPlayer, Controller);
		FTimerHandle TimerHandle;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, SecondsToRespawn, false);
	}
	
}

void ATestGameMode::HandleRespawnPlayer(AController* Controller)
{
	AFirstPersonPlayerController* PlayerController = Cast<AFirstPersonPlayerController>(Controller);
	if (PlayerController)
	{
		const int RandomIndex = FMath::RandRange(0, SpawnPoints.Num() - 1);
		AActor* Spawn = SpawnPoints[RandomIndex];
		if (Spawn)
		{
			UWorld* World = GetWorld();
			if (World)
			{
				const FVector SpawnLocation = Spawn->GetActorLocation();
				const FRotator SpawnRotation = Spawn->GetActorRotation();
				APawn* SpawnedPawn = World->SpawnActor<APawn>(DefaultPawnClass, SpawnLocation, SpawnRotation);
				if (SpawnedPawn)
				{
					PlayerController->Possess(SpawnedPawn);
					PlayerController->ResetHUDOnRespawn();
				}
			}
		}
	}
}
