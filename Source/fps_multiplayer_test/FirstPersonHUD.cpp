// Copyright Epic Games, Inc. All Rights Reserved.

#include "FirstPersonHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "FirstPersonPlayerController.h"

AFirstPersonHUD::AFirstPersonHUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;

	// Set a refence to the health widget class
	static ConstructorHelpers::FClassFinder<UUserWidget> HUDWidgetObj(TEXT("/Game/FirstPersonCPP/Blueprints/BP_HUDWidget"));
	HUDWidgetClass = HUDWidgetObj.Class;
}

void AFirstPersonHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X), (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void AFirstPersonHUD::BeginPlay()
{
	Super::BeginPlay();

	if (HUDWidgetClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			CurrentWidget = CreateWidget<UUserWidget>(World, HUDWidgetClass);
			if (CurrentWidget)
			{
				CurrentWidget->AddToViewport();
			}
		}
	}

	AFirstPersonPlayerController* PlayerController = Cast<AFirstPersonPlayerController>(GetOwningPlayerController());
	if (PlayerController)
	{
		PlayerController->OnPlayerRespawn.AddDynamic(this, &AFirstPersonHUD::OnPlayerRespawn);
	}
}

void AFirstPersonHUD::OnPlayerRespawn()
{
	if (CurrentWidget)
	{
		CurrentWidget->RemoveFromParent();
	}

	if (HUDWidgetClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			CurrentWidget = CreateWidget<UUserWidget>(World, HUDWidgetClass);
			if (CurrentWidget)
			{
				CurrentWidget->AddToViewport();
			}
		}
	}
}
