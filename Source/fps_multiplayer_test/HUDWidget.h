// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class FPS_MULTIPLAYER_TEST_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* HealthBar;

	UPROPERTY(meta = (BindWidget))
	class UProgressBar* MagicBar;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* MagicCooldownText;
	
	void NativeConstruct() override;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:

	/** Timer counter */
	UPROPERTY()
	float MagicCooldownTimer;

	/** Seconds to cool down the magic */
	UPROPERTY()
	float TimeToCooldownMagic;

	/** Timer counter */
	UPROPERTY()
	float MagicChargingTimer;

	/** Seconds to charge the magic */
	UPROPERTY()
	float TimeToChargeMagic;

	UFUNCTION()
	void OnOwnerHealthChanged(float Health, float NormalizedHealth, float DeltaHealth);

	UFUNCTION()
	void OnOwnerChargesMagic();

	UFUNCTION()
	void OnOwnerReleasesMagic();

	UPROPERTY()
	bool bCanTickMagicBar;

	UPROPERTY()
	bool bCanTickMagicCoolDown;
};
