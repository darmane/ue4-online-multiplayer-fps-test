// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FirstPersonHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnHealthChanged, float, Health, float, NormalizedHealth, float, DeltaHealth);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FPS_MULTIPLAYER_TEST_API UFirstPersonHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFirstPersonHealthComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Health)
	float BaseHealth;
	
	UPROPERTY(Replicated, BlueprintReadOnly, Category = Health)
	float Health;

	UPROPERTY(BlueprintReadOnly, Category = Health)
	bool bIsDead;

private:
	// Damage the character. Heal amount must be > 0
	UFUNCTION()
	void HandleOnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(NetMulticast, Reliable)
	void Server_OnTakeAnyDamageMulticast(float CurrentHealth, float NormalizedHealth, float DeltaHealth);

	void SendHealthChanged(float CurrentHealth, float NormalizedHealth, float DeltaHealth);

public:
	UPROPERTY(BlueprintAssignable)
	FOnHealthChanged OnHealthChanged;

	// Heal the character. Heal amount must be > 0
	UFUNCTION(BlueprintCallable)
	void HealCharacter(float HealAmount);

	UFUNCTION(BlueprintCallable)
	float GetNormalizedHealth();

	UFUNCTION(BlueprintCallable)
	float GetHealth();

};
