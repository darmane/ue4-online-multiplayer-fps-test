// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FirstPersonPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerRespawn);

UCLASS()
class FPS_MULTIPLAYER_TEST_API AFirstPersonPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnPlayerRespawn OnPlayerRespawn;
	
	void ResetHUDOnRespawn();

private:
	UFUNCTION(NetMulticast, Reliable)
	void Server_OnPlayerRespawnMulticast();
	
};
