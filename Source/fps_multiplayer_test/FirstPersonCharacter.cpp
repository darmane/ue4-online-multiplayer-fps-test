// Copyright Epic Games, Inc. All Rights Reserved.

#include "FirstPersonCharacter.h"
#include "FirstPersonProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/PawnMovementComponent.h" 
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "FirstPersonHealthComponent.h"
#include "TestGameMode.h"
#include "Net/UnrealNetwork.h"


DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFirstPersonCharacter

AFirstPersonCharacter::AFirstPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));
	Mesh1P->SetOnlyOwnerSee(false);

	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(RootComponent);
	FP_Gun->SetOnlyOwnerSee(false);

	// Create muzzle component for projectiles spawn location
	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	BodyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterBody"));
	BodyMesh->SetupAttachment(FirstPersonCameraComponent);
	BodyMesh->bCastDynamicShadow = false;
	BodyMesh->CastShadow = false;
	BodyMesh->SetOwnerNoSee(true); // The owner should not see this mesh
	BodyMesh->SetOnlyOwnerSee(false);
	BodyMesh->SetRelativeLocation(FVector(-20.f, 20.f, -70.f));
	BodyMesh->SetRelativeScale3D(FVector(.5f, .5f, 1.75));

	MagicArea = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MagicArea"));
	MagicArea->SetupAttachment(GetCapsuleComponent());
	MagicArea->bCastDynamicShadow = false;
	MagicArea->CastShadow = false;
	MagicArea->SetCollisionProfileName("OverlapAllDynamic");
	MagicArea->SetRelativeScale3D(FVector::ZeroVector);
	MagicArea->SetIsReplicated(true);

	HealthComponent = CreateDefaultSubobject<UFirstPersonHealthComponent>(TEXT("HealthComponent"));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Magic setup
	MagicAreaMaxScale = 3.f;
	TimeToChargeMagic = 3.f;
	MagicDamage = 50.f;
	MagicCooldownTime = 5.f;

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P and FP_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.
}

void AFirstPersonCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	// Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Subscribe to health change event in health component
	if (HealthComponent)
	{
		HealthComponent->OnHealthChanged.AddDynamic(this, &AFirstPersonCharacter::OnCharacterHealthChanged);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFirstPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFirstPersonCharacter::OnFire);

	// Bind magic event
	PlayerInputComponent->BindAxis("Magic", this, &AFirstPersonCharacter::OnMagic);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFirstPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFirstPersonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFirstPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFirstPersonCharacter::LookUpAtRate);
}

void AFirstPersonCharacter::OnFire()
{
	// Run only on server
	if (GetLocalRole() < ROLE_Authority)
	{
		Server_OnFire();
		return;
	}

	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator SpawnRotation = GetControlRotation();
			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<AFirstPersonProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}

	// Play fire animation in all clients
	Server_OnFireMulticast();
}

void AFirstPersonCharacter::Server_OnFire_Implementation()
{
	OnFire();
}

bool AFirstPersonCharacter::Server_OnFire_Validate()
{
	return true;
}

void AFirstPersonCharacter::Server_OnFireMulticast_Implementation()
{
	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

bool AFirstPersonCharacter::Server_OnFireMulticast_Validate()
{
	return true;
}

void AFirstPersonCharacter::OnMagic(float Value)
{
	if (Value > 0.f)
	{
		ChargeMagic();
	}
	else
	{
		ReleaseMagic();
	}
}

void AFirstPersonCharacter::ChargeMagic()
{
	if (bIsCoolingDown)
	{
		return;
	}

	if (bIsChargingMagic)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			if (MagicCharge < TimeToChargeMagic)
			{
				const float Scale = MagicAreaMaxScale * FMath::Clamp(MagicCharge / TimeToChargeMagic, 0.f, 1.f);
				MagicArea->SetRelativeScale3D(FVector::OneVector * Scale);
				Server_UpdateMagic(Scale);

				const float DeltaTime = World->DeltaTimeSeconds;
				MagicCharge += DeltaTime;
			}
		}
	}
	else
	{
		MagicCharge = 0.f;
		bIsChargingMagic = true;

		OnChargeMagic.Broadcast();
	}
}

void AFirstPersonCharacter::ReleaseMagic()
{
	if (bIsChargingMagic)
	{
		bIsChargingMagic = false;
		bIsCoolingDown = true;

		UWorld* World = GetWorld();
		if (World)
		{
			FTimerHandle TimerHandle;
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]()
				{
					bIsCoolingDown = false;
				}, MagicCooldownTime, false);
		}

		if (MagicArea)
		{
			Server_ReleaseMagic();
			MagicArea->SetRelativeScale3D(FVector::ZeroVector);
		}

		OnReleaseMagic.Broadcast();
	}
}

void AFirstPersonCharacter::Server_UpdateMagic_Implementation(float Scale)
{
	if (MagicArea)
	{
		// Update area in server
		MagicArea->SetRelativeScale3D(FVector::OneVector * Scale);
	}
}

void AFirstPersonCharacter::Server_ReleaseMagic_Implementation()
{
	if (MagicArea)
	{
		TArray<AActor*> OverlappingActors;
		MagicArea->GetOverlappingActors(OverlappingActors, AFirstPersonCharacter::StaticClass());
		for (AActor* OverlappingActor : OverlappingActors)
		{
			if (OverlappingActor && OverlappingActor != this)
			{
				AFirstPersonCharacter* OtherCharacter = Cast<AFirstPersonCharacter>(OverlappingActor);
				if (OtherCharacter)
				{
					OtherCharacter->TakeDamage(MagicDamage, FDamageEvent(UDamageType::StaticClass()), GetInstigatorController(), this);
				}
			}
		}

		MagicArea->SetRelativeScale3D(FVector::ZeroVector);
	}
}

void AFirstPersonCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFirstPersonCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFirstPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFirstPersonCharacter::OnCharacterHealthChanged(float Health, float NormalizedHealth, float DeltaHealth)
{
	if (Health <= 0.f && !bIsDead)
	{
		bIsDead = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		ATestGameMode* GameMode = Cast<ATestGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->RespawnPlayer(Controller);
		}

		Destroy();
	}
}