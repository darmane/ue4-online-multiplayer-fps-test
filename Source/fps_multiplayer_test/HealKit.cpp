// Fill out your copyright notice in the Description page of Project Settings.


#include "HealKit.h"
#include "Components/BoxComponent.h" 
#include "Components/StaticMeshComponent.h" 
#include "FirstPersonCharacter.h"
#include "FirstPersonHealthComponent.h"


// Sets default values
AHealKit::AHealKit()
{
	Collision = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Collision;
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AHealKit::OnBeginOverlap);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	HealthAmount = 100.f;
}

// Called when the game starts or when spawned
void AHealKit::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHealKit::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// OtherActor is the actor that triggered the event. Check that is not ourself
	if ((OtherActor == nullptr) || (OtherActor == this) || (OtherComp == nullptr))
	{
		return;
	}

	AFirstPersonCharacter* Character = Cast<AFirstPersonCharacter>(OtherActor);
	if (Character)
	{
		if (OtherComp == Character->GetMagicArea())
		{
			return;
		}

		UFirstPersonHealthComponent* HealthComponent = Character->GetHealthComponent();
		if (HealthComponent)
		{
			HealthComponent->HealCharacter(HealthAmount);

			Destroy();
		}
	}
}