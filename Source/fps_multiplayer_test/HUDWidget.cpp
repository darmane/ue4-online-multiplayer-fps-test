// Fill out your copyright notice in the Description page of Project Settings.


#include "HUDWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "FirstPersonCharacter.h"
#include "FirstPersonHealthComponent.h"

void UHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	AFirstPersonCharacter* Character = Cast<AFirstPersonCharacter>(GetOwningPlayerPawn());
	if (Character)
	{
		TimeToCooldownMagic = Character->MagicCooldownTime;
		TimeToChargeMagic = Character->TimeToChargeMagic;

		Character->OnChargeMagic.AddDynamic(this, &UHUDWidget::OnOwnerChargesMagic);
		Character->OnReleaseMagic.AddDynamic(this, &UHUDWidget::OnOwnerReleasesMagic);

		UFirstPersonHealthComponent* HealthComponent = Character->GetHealthComponent();
		if (HealthComponent)
		{
			HealthBar->SetPercent(HealthComponent->GetNormalizedHealth());

			HealthComponent->OnHealthChanged.AddDynamic(this, &UHUDWidget::OnOwnerHealthChanged);
		}
	}
}

void UHUDWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	
	if (bCanTickMagicBar)
	{
		if (MagicChargingTimer < TimeToChargeMagic)
		{
			if (MagicBar)
			{
				const float Percent = FMath::Clamp(MagicChargingTimer / TimeToChargeMagic, 0.f, 1.f);
				MagicBar->SetPercent(Percent);

				MagicChargingTimer += InDeltaTime;
			}
		}
		else
		{
			bCanTickMagicBar = false;
		}
	}
	else if (bCanTickMagicCoolDown)
	{
		if (MagicCooldownTimer > 0.f)
		{
			if (MagicCooldownText)
			{
				MagicCooldownText->SetText(FText::FromString(FString::Printf(TEXT("%d"), (int)MagicCooldownTimer)));
			}

			MagicCooldownTimer -= InDeltaTime;
		}
		else
		{
			bCanTickMagicCoolDown = false;
		}
	}
}

void UHUDWidget::OnOwnerHealthChanged(float Health, float NormalizedHealth, float DeltaHealth)
{
	HealthBar->SetPercent(NormalizedHealth);
}

void UHUDWidget::OnOwnerChargesMagic()
{
	bCanTickMagicBar = true;

	MagicChargingTimer = 0.f;
}

void UHUDWidget::OnOwnerReleasesMagic()
{	
	bCanTickMagicCoolDown = true;
	bCanTickMagicBar = false;

	MagicCooldownTimer = TimeToCooldownMagic;

	if (MagicBar)
	{
		MagicBar->SetPercent(0.f);
	}

	if (MagicCooldownText)
	{
		MagicCooldownText->SetText(FText::FromString(FString::Printf(TEXT("%d"), (int)MagicCooldownTimer)));
	}
}



