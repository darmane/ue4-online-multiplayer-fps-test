# UE4 Online Multiplayer FPS

![](images/hero.gif)

This is a quick test project that starts with the template "First Person" provided by Epic Games. It is an online multiplayer first person shooter in which we can shoot at opponents or apply damage to them with a special area attack, all fully replicated betweens the server and the clients.

### Improvements needed
- Change the file structure in the Content Browser to a standard one with a better organization than the default one in Unreal Engine.
- In the HUD widget, change the area attack bar update system to try not to use the tick if possible.
- Improve the naming of variables and functions following standard style guide and clean, scalable and maintainable code, both of those implemented by me and those implemented by Epic Games in the template.
- Refactor the functionalities to optimize the code, using the profiler to monitor the performance of the functions.
- Use interfaces to reduce cross-dependencies resulting from casting.
- Redesign communications between the server and the clients to be more separated from the rest of the functionalities and to be easier to maintain and scale the code.

## Stack
- Unreal Engine 4.25
- C++

## Tutorial
### Run the project
- Compile the project and open it.
- In the editor's toolbar, open Play options.
- Set "Number of Players" to 2 and "Net Mode" to "Play as Client".
- Play in "New Editor Window (PIE)".

### Controls
- Shot: Mouse left click.
- Area attack: Mouse right click.
- Move character: W,A,S,D.
- Roatation: Mouse Axis.

## License
MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" basis and without warranty of any kind. Use it at your own risk.